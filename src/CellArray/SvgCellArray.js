import React, {Component} from "react";
import './SvgCellArray.css'

const cellColor = (state) => {
    let color;
    switch (state) {
        case 1:
            color = 'red';
            break;
        case 2:
            color = 'blue';
            break;
        case 3:
            color = 'green';
            break;
        default:
            color = 'aquamarine';
            break;
    }

    return color;
}

const cellId = (cell) => {
    return cell.position.column + "_" + cell.position.row;
}

class SvgCellArray extends Component {
    constructor(props) {
        super(props)
    }

    renderCell = (cell) => {
        const style = {
            "fill": cellColor(cell.state)
        }
        return (
            <rect
                onMouseMove={this.props.cellOnClickHandler}
                id={cellId(cell)}
                key={cellId(cell)}
                x={cell.position.row * 12} y={cell.position.column * 12}
                width="10" height="10"
                style={style}/>
        )
    }

    render() {

        const array = (
            <svg className="SvgCellArray" width={this.props.cellArrayModel.size.rows * 12}
                 height={this.props.cellArrayModel.size.columns * 12}>
                {
                    this.props.cellArrayModel.cellArray.map((row, index) => {
                        return (row.map((cell) => {
                            return this.renderCell(cell)
                        }))
                    })
                }
            </svg>
        );

        return array;
    }
}

export default SvgCellArray