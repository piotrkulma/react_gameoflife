/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/
import Neighborhood from './Neighborhood'

class MooreNeighborhood extends Neighborhood {
    countNeighbors(array, row, column) {
        let counter = 0;

        for (let i of [-1, 0, 1]) {
            for (let j of [-1, 0, 1]) {
                if (i === 0 && j === 0) {
                    continue;
                }
                if (this.isCellAlive(array, row + i, column + j)) {
                    counter++;
                }
            }
        }

        return counter;
    }

    getAllNeighbors(array, row, column) {
        let cells = [];

        for (let i of [-1, 0, 1]) {
            for (let j of [-1, 0, 1]) {
                if (i === 0 && j === 0) {
                    continue;
                }
                if (this.isCellAlive(array, row + i, column + j)) {
                    cells.push(array.cellArray[row + i][column + j].deepCopy());
                }
            }
        }

        return cells;
    }
}

export default MooreNeighborhood