/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/

class Neighborhood {
    constructor(spaceTransformer, deadState) {
        this.spaceTransformer = spaceTransformer;
        this.deadState = deadState;
    }

    countNeighbors(array, row, column) {
    }

    getAllNeighbors(array, row, column) {
    }

    isCellAlive(array, row, column) {
        let cell = this.spaceTransformer.getCell(array, row, column)
        if(cell !== null && cell.state !== this.deadState) {
            return true;
        }

        return false;
    }
}


export default Neighborhood