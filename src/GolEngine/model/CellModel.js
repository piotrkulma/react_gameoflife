/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/

class CellModel {
    constructor(state, row, column) {
        this.state = state;
        this.position = {
            row: row,
            column: column
        }
    }

    deepCopy() {
        return new CellModel(this.state, this.position.row, this.position.column)
    }
}

export default CellModel;