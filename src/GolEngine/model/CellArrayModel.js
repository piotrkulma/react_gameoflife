/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/
import CellModel from "./CellModel";

class CellArrayModel {
    constructor(rows, columns) {
        this.size = {
            rows: rows,
            columns: columns
        }
        this.cellArray = this.initCellArray(this.size.rows, this.size.columns);
    }

    initCellArray(rows, columns, defaultState = 0) {
        let array = [];
        for (let i = 0; i < rows; i++) {
            array.push(new Array(columns));
            for (let j = 0; j < columns; j++) {
                array[i][j] = new CellModel(defaultState, i, j);
            }
        }

        return array;
    }

    getCell(row, column) {
        return this.cellArray[row][column].deepCopy();
    }

    deepCopy() {
        let deepCopy = new CellArrayModel(this.size.rows, this.size.columns)

        for (let i = 0; i < this.size.rows; i++) {
            for (let j = 0; j < this.size.columns; j++) {
                deepCopy.cellArray[i][j] = this.cellArray[i][j].deepCopy()
            }
        }

        return deepCopy;
    }
}

export default CellArrayModel;