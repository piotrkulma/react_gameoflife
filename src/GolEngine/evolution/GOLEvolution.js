/*eslint-disable no-unused-vars*/
import React from'react'
/*eslint-disable no-unused-vars*/
import Evolution from "./Evolution";

class GOLEvolution extends Evolution {
    statusAfterEvolution(cellArrayModel, i, j) {
        let actualState = cellArrayModel.getCell(i, j).state;
        let neighbors = this.neighborhood.countNeighbors(cellArrayModel, i, j);
        let newStatus;

        if(actualState === 0 && neighbors === 3) {
            newStatus = 1;
        } else if(actualState === 1 && (neighbors === 2 || neighbors === 3)) {
            newStatus = 1;
        } else {
            newStatus = 0;
        }

        return newStatus;
    }
}

export default GOLEvolution