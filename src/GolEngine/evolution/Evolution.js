/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/

class Evolution {
    constructor(neighborhood) {
        this.neighborhood = neighborhood;
    }

    evolve(model) {
        let modelCopy = model.deepCopy()
        for (let i = 0; i < model.size.rows; i++) {
            for (let j = 0; j < model.size.columns; j++) {
                modelCopy.cellArray[i][j].state = this.statusAfterEvolution(model, i, j)
            }
        }

        return modelCopy
    }

    statusAfterEvolution(cellArrayModel, i, j) {
    }
}

export default Evolution