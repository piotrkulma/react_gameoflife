/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/
import Evolution from "./Evolution";

class WireworldEvolution extends Evolution {
    WIRE_STATE = 1;
    ELECTRON_HEAD_STATE = 2;
    ELECTRON_TAIL_STATE = 3;

    statusAfterEvolution(cellArrayModel, i, j) {
        let actualState = cellArrayModel.getCell(i, j).state;
        let neighbors = this.neighborhood.getAllNeighbors(cellArrayModel, i, j);
        let electronHeads = neighbors.filter(cell => cell.state === this.ELECTRON_HEAD_STATE).length;
        let newStatus = actualState;

        if (actualState === this.ELECTRON_HEAD_STATE) {
            newStatus = this.ELECTRON_TAIL_STATE;

        } else if (actualState === this.ELECTRON_TAIL_STATE) {
            newStatus = this.WIRE_STATE

        } else if (actualState === this.WIRE_STATE && (electronHeads === 1 || electronHeads === 2)) {
            newStatus = this.ELECTRON_HEAD_STATE;
        }

        return newStatus;
    }
}

export default WireworldEvolution