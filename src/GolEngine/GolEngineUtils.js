/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/

import CellArrayModel from "./model/CellArrayModel"
import InfiniteSpaceTransformer from "./space/InfiniteSpaceTransformer"
import ClosedSpaceTransformer from "./space/ClosedSpaceTransformer"
import MooreNeighborhood from "./neighborhood/MooreNeighborhood"
import GOLEvolution from "./evolution/GOLEvolution"
import NeumannNeighborhood from "./neighborhood/NeumannNeighborhood"
import WireworldEvolution from "./evolution/WireworldEvolution";

const modifyCellArrayModel = (cellArrayModel, columns, rows) => {
    let model = new CellArrayModel(columns, rows)

    if (cellArrayModel !== null) {
        for (let i = 0; i < model.size.rows; i++) {
            for (let j = 0; j < model.size.columns; j++) {
                if (i < cellArrayModel.size.rows && j < cellArrayModel.size.columns) {
                    model.cellArray[i][j] = cellArrayModel.cellArray[i][j];
                }
            }
        }
    }

    return model
}

const createSpaceTransformer = (val) => {
    switch (val) {
        case 1:
            return new InfiniteSpaceTransformer()
        default:
            return new ClosedSpaceTransformer()
    }
}

const createNeighborhood = (spaceTransformer, val) => {
    switch (val) {
        case 1:
            return new NeumannNeighborhood(spaceTransformer, 0)
        default:
            return new MooreNeighborhood(spaceTransformer, 0)
    }
}

const createEvolutionAlgorithm = (neighborhood, val) => {
    switch (val) {
        case 1:
            return new WireworldEvolution(neighborhood);
        default:
            return new GOLEvolution(neighborhood)
    }
}

export {modifyCellArrayModel, createSpaceTransformer, createNeighborhood, createEvolutionAlgorithm}