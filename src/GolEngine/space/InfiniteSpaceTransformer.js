/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/
import SpaceTransformer from "./SpaceTransformer";

class InfiniteSpaceTransformer extends SpaceTransformer {
    isCellExists(cellArrayModel, row, column) {
        return true;
    }

    findCellXPosition(cellArrayModel, pos) {
        let newPos = pos;

        if (pos < 0) {
            newPos = cellArrayModel.size.rows - 1;
        } else if (pos >= cellArrayModel.size.rows) {
            newPos = 0;
        }

        return newPos;
    }

    findCelYPosition(cellArrayModel, pos) {
        let newPos = pos;

        if (pos < 0) {
            newPos = cellArrayModel.size.columns - 1;
        } else if (pos >= cellArrayModel.size.columns) {
            newPos = 0;
        }

        return newPos;
    }
}

export default InfiniteSpaceTransformer;