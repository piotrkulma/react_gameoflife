/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/

class SpaceTransformer {
    isCellExists(cellArrayModel, row, column) {
    }

    findCellXPosition(pos) {
    }

    findCelYPosition(pos) {
    }

    getCell(cellArrayModel, row, column) {
        if (this.isCellExists(cellArrayModel, row, column)) {
            return cellArrayModel.cellArray[this.findCellXPosition(cellArrayModel, row)][this.findCelYPosition(cellArrayModel, column)].deepCopy();
        }

        return null;
    }
}

export default SpaceTransformer;