/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/
import SpaceTransformer from "./SpaceTransformer";

class ClosedSpaceTransformer extends SpaceTransformer {
    isCellExists(cellArrayModel, row, column) {
        if (row >= 0 && row < cellArrayModel.size.rows &&
            column >= 0 && column < cellArrayModel.size.columns) {
            return true;
        }

        return false;
    }

    findCellXPosition(cellArrayModel, pos) {
        return pos;
    }

    findCelYPosition(cellArrayModel, pos) {
        return pos;
    }
}

export default ClosedSpaceTransformer;