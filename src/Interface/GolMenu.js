import React, {Component} from 'react'

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import './GolMenu.css'

class GolMenu extends Component {
    state = {
        showMenu: false,
        anchorElementAlgorithm: null,
        anchorElementEdges: null,
        anchorElementNeighborhood: null,
        anchorElementCellType: null

    }

    algorithmClickHandler = (event) => {
        this.setState({anchorElementAlgorithm: event.target})
    };

    algorithmMenuCloseHandler = (val) => {
        if (val !== null) {
            this.props.evolutionAlgorithmChangedHandler(val)
        }
        this.setState({anchorElementAlgorithm: null})
    };

    edgesClickHandler = (event) => {
        this.setState({anchorElementEdges: event.target})
    };

    edgesMenuCloseHandler = (val = null) => {
        if (val !== null) {
            this.props.spaceTransformerChangedHandler(val)
        }
        this.setState({anchorElementEdges: null})
    };

    neighborhoodClickHandler = (event) => {
        this.setState({anchorElementNeighborhood: event.target})
    };

    neighborhoodMenuCloseHandler = (val) => {
        if (val !== null) {
            this.props.neighborhoodChangedHandler(val)
        }
        this.setState({anchorElementNeighborhood: null})
    };

    cellTypeClickHandler = (event) => {
        this.setState({anchorElementCellType: event.target})
    };

    cellTypeMenuCloseHandler = (val) => {
        if (val !== null) {
            this.props.cellTypeChangedHandler(val)
        }
        this.setState({anchorElementCellType: null})
    };

    render() {
        return (
            <div className="GolMenu">
                <button onClick={this.algorithmClickHandler}>Game algorithm</button>
                <button onClick={this.edgesClickHandler}>Edges</button>
                <button onClick={this.neighborhoodClickHandler}>Neighborhood</button>
                <button onClick={this.cellTypeClickHandler}>Cell type</button>

                <Menu id="gameAlgorithmMenu"
                      anchorEl={this.state.anchorElementAlgorithm}
                      keepMounted
                      open={Boolean(this.state.anchorElementAlgorithm)}
                      onClose={() => this.algorithmMenuCloseHandler(null)}>
                    <MenuItem
                        selected={this.props.evolutionAlgorithmType === 0}
                        onClick={() => this.algorithmMenuCloseHandler(0)}>Conaway's Game of life</MenuItem>
                    <MenuItem
                        selected={this.props.evolutionAlgorithmType === 1}
                        onClick={() => this.algorithmMenuCloseHandler(1)}>Wireworld</MenuItem>
                </Menu>

                <Menu id="edgesMenu"
                      anchorEl={this.state.anchorElementEdges}
                      keepMounted
                      open={Boolean(this.state.anchorElementEdges)}
                      onClose={() => this.edgesMenuCloseHandler(null)}>
                    <MenuItem
                        selected={this.props.spaceTransformerType === 0}
                        onClick={() => this.edgesMenuCloseHandler(0)}>Blocked</MenuItem>
                    <MenuItem
                        selected={this.props.spaceTransformerType === 1}
                        onClick={() => this.edgesMenuCloseHandler(1)}>Infinity</MenuItem>
                </Menu>

                <Menu id="neighborhoodMenu"
                      anchorEl={this.state.anchorElementNeighborhood}
                      keepMounted
                      open={Boolean(this.state.anchorElementNeighborhood)}
                      onClose={() => this.neighborhoodMenuCloseHandler(null)}>
                    <MenuItem
                        selected={this.props.neighborhoodType === 0}
                        onClick={() => this.neighborhoodMenuCloseHandler(0)}>Moore's</MenuItem>
                    <MenuItem
                        selected={this.props.neighborhoodType === 1}
                        onClick={() => this.neighborhoodMenuCloseHandler(1)}>von Neumann's</MenuItem>
                </Menu>

                <Menu id="cellTypeMenu"
                      anchorEl={this.state.anchorElementCellType}
                      keepMounted
                      open={Boolean(this.state.anchorElementCellType)}
                      onClose={() => this.cellTypeMenuCloseHandler(null)}>
                    <MenuItem
                        selected={this.props.cellType === 0}
                        onClick={() => this.cellTypeMenuCloseHandler(0)}>0</MenuItem>
                    <MenuItem
                        selected={this.props.cellType === 1}
                        onClick={() => this.cellTypeMenuCloseHandler(1)}>1</MenuItem>
                    <MenuItem
                        selected={this.props.cellType === 2}
                        onClick={() => this.cellTypeMenuCloseHandler(2)}>2</MenuItem>
                </Menu>
            </div>
        )
    }
}

export default GolMenu;