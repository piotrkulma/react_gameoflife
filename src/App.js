import React, {Component} from 'react'

import GolMenu from './Interface/GolMenu'
import SvgCellArray from './CellArray/SvgCellArray'
import './App.css'

import {
    createEvolutionAlgorithm,
    createNeighborhood,
    createSpaceTransformer,
    modifyCellArrayModel
} from './GolEngine/GolEngineUtils'

class App extends Component {
    state = {
        cellArrayModel: null,
        spaceTransformer: null,
        neighborhood: null,
        evolutionAlgorithm: null,
        evolutionTimerEnabled: false,
        evolutionAlgorithmType: 0,
        spaceTransformerType: 1,
        neighborhoodType: 0,
        cellType: 1,
        initialized: false,
        mouseDown: false,
        columns: 30,
        rows: 20,
    }

    updateGameConfig = (config) => {
        this.setState(config, () => {
            this.setState(
                {cellArrayModel: modifyCellArrayModel(this.state.cellArrayModel, this.state.columns, this.state.rows)}, () => {
                    this.setState(
                        {spaceTransformer: createSpaceTransformer(this.state.spaceTransformerType)}, () => {
                            this.setState(
                                {neighborhood: createNeighborhood(this.state.spaceTransformer, this.state.neighborhoodType)}, () => {
                                    this.setState(
                                        {evolutionAlgorithm: createEvolutionAlgorithm(this.state.neighborhood, this.state.evolutionAlgorithmType)}
                                    )
                                })
                        })
                })
        })
    }

    cellTypeChangedHandler = (val) => {
        this.updateGameConfig({cellType: val})
    }

    evolutionAlgorithmChangedHandler = (val) => {
        this.updateGameConfig({evolutionAlgorithmType: val})
    }

    spaceTransformerChangedHandler = (val) => {
        this.updateGameConfig({spaceTransformerType: val})
    }

    neighborhoodChangedHandler = (val) => {
        this.updateGameConfig({neighborhoodType: val})
    };

    cellOnMouseDownHandler = () => {
        this.setState({mouseDown: true})
    }

    cellOnMouseUpHandler = () => {
        this.setState({mouseDown: false})
    }

    cellOnClickHandler = (event) => {
        event.preventDefault()
        if (!this.state.mouseDown) {
            return
        }

        const [j, i] = event.target.id.split('_')
        let cellArrayModel = this.state.cellArrayModel.deepCopy()
        cellArrayModel.cellArray[i][j].state = this.state.cellType
        this.setState({cellArrayModel: cellArrayModel})
    }

    evolutionStepHandler = () => {
        const evolvedCellArrayModel = this.state.evolutionAlgorithm.evolve(this.state.cellArrayModel);

        this.setState({cellArrayModel: evolvedCellArrayModel})
    }

    evolutionTimerHandler = () => {
        if (!this.state.initialized) {
            this.updateGameConfig({});
            this.setState({initialized: true});
        }

        if (this.state.evolutionTimerEnabled) {
            this.evolutionStepHandler();
        }
    }

    componentDidMount() {
        this.interval = setInterval(() => this.evolutionTimerHandler(), 200);
    }

    evolutionTimerEnabledHandler = () => {
        this.setState({evolutionTimerEnabled: !this.state.evolutionTimerEnabled})
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        if (!this.state.initialized) {
            return <div></div>
        }

        return (
            <div className="App" onMouseDown={() => this.cellOnMouseDownHandler()}
                 onMouseUp={() => this.cellOnMouseUpHandler()}>
                <GolMenu
                    evolutionAlgorithmType={this.state.evolutionAlgorithmType}
                    spaceTransformerType={this.state.spaceTransformerType}
                    neighborhoodType={this.state.neighborhoodType}
                    cellType={this.state.cellType}
                    evolutionAlgorithmChangedHandler={this.evolutionAlgorithmChangedHandler}
                    spaceTransformerChangedHandler={this.spaceTransformerChangedHandler}
                    neighborhoodChangedHandler={this.neighborhoodChangedHandler}
                    cellTypeChangedHandler={this.cellTypeChangedHandler}
                />

                <SvgCellArray
                    cellOnClickHandler={(event) => this.cellOnClickHandler(event)}
                    cellArrayModel={this.state.cellArrayModel}/>
{/*                <CellArray
                    cellOnClickHandler={(event) => this.cellOnClickHandler(event)}
                    cellArrayModel={this.state.cellArrayModel}/>*/}

                <div>
                    <button onClick={() => this.evolutionStepHandler()}>Step evolution</button>
                    <button
                        onClick={() => this.evolutionTimerEnabledHandler()}>{this.state.evolutionTimerEnabled ? 'Stop' : 'Start'}</button>
                </div>

            </div>
        );
    }
}

export default App;
